package ru.example.network;

import java.io.IOException;

public interface TCPConnectionListener {

    void onConnectionIsReady(TCPConnection tcpConnection);

    void onReceiveString(TCPConnection tcpConnection, String string);

    void onDisconnect(TCPConnection tcpConnection);

    void onException(TCPConnection tcpConnection, IOException e);

}
